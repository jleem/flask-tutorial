#!/usr/bin/env python

"""
main.py
Description:
Mar 12, 2018
"""
# Python imports that could be useful
import sys,os
import numpy as np
from collections import defaultdict

# Flask imports
from flask import Flask, request, url_for, render_template, Markup 

# Initialise the webapp as "webapp"
app = Flask(__name__)
app.jinja_env.trim_blocks = True
app.jinja_env.lstrip_blocks = True # https://pythonadventures.wordpress.com/2015/01/29/flask-jinja2-dont-print-empty-lines/


### FLASK functions
### Generic formula ###
# @app.route('/DESTINATION')
# def name_of_function(parameters):
#     stuff = do_something(parameters)
#     return render_template( stuff_to_send = stuff )
### End of generic  ###

# Create a decorator for this function that routes to the folder "/" for the website.
@app.route('/')
def home():
    return render_template("base.html",
                           title="Hello there!",
                           subtitle ="A Flask/D3 tutorial brought to you by Jin and Flo.",
                           tutorial = True)

@app.route('/error')
def error_message():
    return render_template("base.html",
            title = "HTML 404: ERROR PAGE!",
            subtitle = "You probably set the wrong parameters for the web page.")

@app.route('/numbers')
def genNumbers():
    """
    Generate 100 numbers and convert into string.
    """
    numbers = [ str(n) for n in range(100) ]
    out     = " ".join(numbers)
    return out

@app.route('/animal', methods = ['GET'])
def viewAnimal():
    """
    Process arguments from the GET request and do something with it.
    """
    args = request.args
    if 'species' in args:
        species = args['species']
        return "Hi, I am a %s" % species
    else:
        return "ERROR, invalid arguments!"

@app.route('/intro', methods = ['GET'])
def introduction():
    """
    To make this page 'work', try http://127.0.0.5000/intro?name=chris
    """
    args = request.args
    if 'name' in args:
        name = args['name'] # Get the name parameter from the GET request
    else:
        return "No name supplied!"

    return render_template("skeleton.html", audience = name)

@app.route('/lottery', methods = ['GET'])
def generate_lottery():
    args = request.args
    if 'name' in args:
        name = args['name'] # Get the name parameter from the GET request
    else:
        return "No name supplied!"

    # Get a random set of integers
    np.random.seed(0)
    random_numbers = np.random.randint(0, 60, size = 7)

    return render_template('skeleton_lottery.html', audience = name, lottery = random_numbers)

@app.route("/statements")
def ifFor():
    return render_template("skeleton_statements.html", boolean = True, myList = range(3))

@app.route("/amino")
def aa():
    amino_acids = "ACDEFGHIKLMNPQRSTVWY"
    #output = []

    random_integers = np.random.randint(0, 20, size = 100)
    string = "".join([ amino_acids[i] for i in random_integers ])
    output = [ string[i:i+80] for i in range(0, len(random_integers), 80) ]
    return render_template("skeleton_amino.html", protein = output)

#### Advanced stuff
### Python functions and constants that could be useful
def parse_data(csvfile, splitter=';'):
    # Get the header, split by semicolon.
    header = csvfile.readline().strip().split(splitter)

    # This is a dictionary where, as a default, if the key does not exist,
    # we create a new list straight away!
    dictionary = defaultdict(list)

    # Iterate through the csvfile.
    for line in csvfile:
        # Strip any newline characters
        tokens = line.strip().split(splitter)
        for i, t in enumerate(tokens):
            # The ith token of header corresponds to the ith header element;
            # append to it.
            dictionary[ header[i] ].append( t )

    return dictionary

fh   = open("static/output_presidency.csv")
data = parse_data(fh)

# Convert text to ascii, then utf-8
text      = list(data['text'])
retweets  = [ int(t) for t in data['retweets'] ]
tweet_id  = data['id']

# pivot to have ID and text
id_text = dict(zip(tweet_id, text))
# pivot to have index and text and id
index_t = dict([ (i, (tweet_id[i], text[i], retweets[i]) ) for i in range(len(tweet_id)) ])

# counts
sorted_counts = sorted(index_t.items(), key = lambda z: z[1][2])


@app.route("/stdout", methods = ["GET"])
def generate_tweet():
    tweet_id = request.args['id']
    if tweet_id in id_text:
        return id_text[tweet_id]
    else:
        return error_message()

@app.route("/analysis", methods=["GET"])
def analyse():

    args  = request.args
    # If we specify the number of rows, then use that. Otherwise, default to 20.
    if 'nrows' in args:
        nrows = int(args['nrows'])
    else:
        nrows = 20

    try:
        # Generate a table
        table = "<tr><th>Tweet</th><th>Num retweets</th><th>Tweet link</th></tr>"
        filtered_data = reversed(sorted_counts[-nrows:])

        for index, data_tuple in filtered_data:
            tweet_id, tweet_str, tweet_rt = data_tuple
            table += "<tr><td>%s</td><td width='10%%'>%d</td><td width='10%%'><a href='%s'>%s</a></td></tr>" % ( tweet_str[:80]+"...", tweet_rt, url_for('generate_tweet', id = tweet_id) , tweet_id )

        # the Markup function converts a string into HTML code
        # basically, the HTML tags that we have in the "table" variable are rendered as HTML, rather than as string
        return render_template("analysis.html",
            title = "Twitter summary stats",
            table = Markup(table))

    except Exception as e:
        return error_message()

    return error_message()
