# FLASK Coding tutorial

This is a tutorial to get you started in writing a website using Flask.

By Jinwoo Leem (jinwoo [ dOt ] leem /at\ gee mail [ DoT ] com) 

## Requirements ##
* Flask
* python 2.7x
* Some basic knowledge of HTML, JavaScript

To get started, run

```FLASK_APP=main.py flask run```

Then access the web page (usually ```http://127.0.0.1:5000```) and run through the tutorial. 
